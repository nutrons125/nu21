package frc.robot;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;

import edu.wpi.first.wpilibj.Servo;

public class Intake {
	private static final int ID = 26;
	private static Intake instance;
	private CANSparkMax intakeMotor;
	private Servo intakeServo;

	private Intake() {
		intakeMotor = new CANSparkMax(ID, CANSparkMaxLowLevel.MotorType.kBrushless);
		intakeMotor.setSmartCurrentLimit(30);
		intakeServo = new Servo(0);
	}

	public static Intake getInstance() {
		return instance = instance == null ? new Intake() : instance;
	}

	public void setServo(double value){
		intakeServo.set(value);
	}

	public void set(double speed) {
		intakeMotor.set(speed);
	}
}
