package frc.robot.utils;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import java.io.*;
import java.util.Base64;

public class SmartObject {
	private final String key;
	
	public SmartObject(String key) {
		this.key = key;
		
	}
	
	public void setValue(Object o) {
		SmartDashboard.putRaw(key, encodeString(o));
	}
	
	public Object getValue() {
		try {
			byte[] barray = Base64.getDecoder().decode(SmartDashboard.getRaw(key, new byte[0]));
			ByteArrayInputStream byteArrayIn = new ByteArrayInputStream(barray);
			ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayIn);
			return objectInputStream.readObject();
		} catch(IOException | ClassNotFoundException ignored) {
		}
		
		return new byte[0];
	}
	
	private byte[] encodeString(Object o) {
		try {
			var byteOutputStream = new ByteArrayOutputStream();
			var objectOut = new ObjectOutputStream(byteOutputStream);
			objectOut.writeObject(o);
			objectOut.close();
			return Base64.getEncoder().encode(byteOutputStream.toByteArray());
		} catch(IOException ignored) {
		}
		return new byte[0];
	}
}
