package frc.robot.utils;
public class Debouncer {
    private int loopCounter = 0;
    private int minimumLoops;

    public Debouncer(double minimumSeconds) {
        this.minimumLoops = (int) (Math.ceil(minimumSeconds / 0.02));
    }

    public boolean get() {
        return this.loopCounter > this.minimumLoops;
    }

    public void update(boolean conditional) {
        this.loopCounter = conditional ? loopCounter + 1 : 0;
    }
}
