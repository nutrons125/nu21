package frc.robot.utils;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class SmartBoolean {
	private final String key;
	private boolean defaultValue = false;
	
	public SmartBoolean(String key) {
		this.key = key == null ? "SmartString key is null" : key;
	}
	
	public SmartBoolean(String key, boolean initialValue) {
		this.key = key == null ? "SmartString key is null" : key;
		setValue(initialValue);
	}
	
	public SmartBoolean(String key, boolean initialValue, boolean defaultValue) {
		this.key = key == null ? "SmartString key is null" : key;
		setDefaultValue(defaultValue).setValue(initialValue);
	}
	
	public SmartBoolean setDefaultValue(boolean defaultValue) {
		this.defaultValue = defaultValue;
		return this;
	}
	
	public void setValue(boolean value) {
		SmartDashboard.putBoolean(key, value);
	}
	
	public boolean getValue() {
		return SmartDashboard.getBoolean(key, defaultValue);
	}
}
