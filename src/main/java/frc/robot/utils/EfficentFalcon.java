package frc.robot.utils;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.motorcontrol.can.TalonFXConfiguration;

/**
 * Class for reducing CAN bus usage among Falcons
 */
public class EfficentFalcon extends TalonFX {
	private TalonFXControlMode localControlMode;
	private double localSet = 0;
	private double kConversionFeetPerTick;
	private double kConversionFeetPerSecondPerTickPer100ms;

	/**
	 * Constructor
	 *
	 * @param deviceNumber [0,62]
	 */
	public EfficentFalcon(int deviceNumber) {
		super(deviceNumber);
		configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor);
	}

	public void setPositionConversionFactor(double factor) {
		kConversionFeetPerTick = factor;
	}

	public void setVelConversionFactor(double factor) {
		kConversionFeetPerSecondPerTickPer100ms = factor;
	}

	public double getVelocity() {
		return getSelectedSensorVelocity() * kConversionFeetPerSecondPerTickPer100ms
				* ((getInverted()) ? 1 : 1);
	}


	public double getPosition() {
		return this.getSensorCollection().getIntegratedSensorPosition() * kConversionFeetPerTick;
	}

	public void setVelocity(double velMetPerSec) {
		set(TalonFXControlMode.Velocity, velMetPerSec / kConversionFeetPerSecondPerTickPer100ms);
	}

	public double getVelErrorMetPerSec() {
		return getClosedLoopError();
	}

	/**
	 * Sets the appropriate output on the talon, depending on the mode.
	 *
	 * @param mode  The output mode to apply.
	 *              In PercentOutput, the output is between -1.0 and 1.0, with 0.0 as stopped.
	 *              In Current mode, output value is in amperes.
	 *              In Velocity mode, output value is in position change / 100ms.
	 *              In Position mode, output value is in encoder ticks or an analog value,
	 *              depending on the sensor.
	 *              In Follower mode, the output value is the integer device ID of the talon to
	 *              duplicate.
	 * @param value The setpoint value, as described above.
	 *              <p>
	 *              <p>
	 *              Standard Driving Example:
	 *              _talonLeft.set(ControlMode.PercentOutput, leftJoy);
	 */
	@Override
	public void set(TalonFXControlMode mode, double value) {
		if (localControlMode != mode || localSet != value)
			super.set(localControlMode = mode, localSet = value);
	}
}
