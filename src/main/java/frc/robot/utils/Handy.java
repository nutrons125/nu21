package frc.robot.utils;

import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;

public class Handy {
	public static double f2m(double feet) {
		return feet / 3.281;
	}

	public static double m2f(double meters) {
		return meters * 3.281;
	}

	public static Pose2d makePose(double x, double y, double deg) {
		return new Pose2d(new Translation2d(x, y), Rotation2d.fromDegrees(deg));
	}

	public static Translation2d makeTranslationM(double x, double y) {
		return new Translation2d(x, y);
	}

	public static Translation2d makeTranslationF(double x, double y) {
		return new Translation2d(f2m(x), f2m(y));
	}

	public static Pose2d poseMtoPoseF(Pose2d poseMeters) {
		return new Pose2d(new Translation2d(m2f(poseMeters.getX()), m2f(poseMeters.getY())), poseMeters.getRotation());
	}

	public static Pose2d poseFtoPoseM(Pose2d poseMeters) {
		return new Pose2d(new Translation2d(f2m(poseMeters.getX()), f2m(poseMeters.getY())), poseMeters.getRotation());
	}
}
