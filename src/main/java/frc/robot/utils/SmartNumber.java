package frc.robot.utils;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class SmartNumber {
	private final String key;
	private double defaultValue = 0;
	
	public SmartNumber(String key) {
		this(key, Double.NaN);
	}
	
	public SmartNumber(String key, double initialValue) {
		this(key, initialValue, Double.NaN);
	}
	
	public SmartNumber(String key, double initialValue, double defaultValue) {
		this.key = key;
		
		if(initialValue == initialValue)
			setValue(initialValue);
		
		if(defaultValue == defaultValue)
			setDefaultValue(defaultValue);
	}
	
	public void setDefaultValue(double value) {
		this.defaultValue = value;
	}
	
	public void setValue(double value) {
		SmartDashboard.putNumber(key, value);
	}
	
	public double getValue() {
		return SmartDashboard.getNumber(key, defaultValue);
	}
	
	public double getValue(double defaultValue) {
		return SmartDashboard.getNumber(key, defaultValue);
	}
}
