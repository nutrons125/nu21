package frc.robot.autos;

import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.PathFollower;
import frc.robot.RobotPath;
import frc.robot.autos.commands.PathFollowCommand;
import frc.robot.autos.commands.SimpleOrbitCommand;

import static frc.robot.utils.Handy.*;

public class Barrel extends Auto {
	@Override
	public void generate() {
		// Start -> First Orbit
		double firstOrbitVel = f2m(7.5);
		Pose2d startToFirstOrbitStart = makePose(0, 0, 0);
		Pose2d startToFirstOrbitEnd = makePose(f2m(8.9), 0, 0);
		RobotPath startToFirstOrbitPath = new RobotPath(
				startToFirstOrbitStart,
				startToFirstOrbitEnd
		);
//		startToFirstOrbitPath.setEndVelocity(10);
		Trajectory startToFirstOrbitTrajectory = startToFirstOrbitPath.generateTrajectory();
		PathFollower startToFirstOrbitFollower = new PathFollower();
		startToFirstOrbitFollower.setTrajectory(startToFirstOrbitTrajectory);
		PathFollowCommand startToFirstOrbitCommand = new PathFollowCommand(startToFirstOrbitFollower);

		Translation2d firstOrbitOffset = makeTranslationF(0, -2.5);
		SimpleOrbitCommand firstOrbitCommand = new SimpleOrbitCommand(firstOrbitOffset, -356 + 25);
		firstOrbitCommand.setSpinVelocity(firstOrbitVel);
		firstOrbitCommand.setStopOnEnd(false);


		// First Orbit -> Second Orbit
		Pose2d firstOrbitToSecondOrbitStart = makePose(f2m(9), 0, 0);
		Pose2d firstOrbitToSecondOrbitEnd = makePose(f2m(16.5), 0, 0);
		RobotPath firstOrbitToSecondOrbitPath = new RobotPath(
				firstOrbitToSecondOrbitStart,
				firstOrbitToSecondOrbitEnd
		);
		firstOrbitToSecondOrbitPath.setStartVel(firstOrbitVel);
//		firstOrbitToSecondOrbitPath.setEndVelocity(firstOrbitVel);
		Trajectory firstOrbitToSecondOrbitTrajectory = firstOrbitToSecondOrbitPath.generateTrajectory();
		PathFollower firstOrbitToSecondOrbitFollower = new PathFollower();
		firstOrbitToSecondOrbitFollower.setTrajectory(firstOrbitToSecondOrbitTrajectory);
		PathFollowCommand firstOrbitToSecondOrbitCommand = new PathFollowCommand(firstOrbitToSecondOrbitFollower);

		Translation2d secondOrbitOffset = makeTranslationF(0, 2.5);
		SimpleOrbitCommand secondOrbitCommand = new SimpleOrbitCommand(secondOrbitOffset, 315 - 35);
		secondOrbitCommand.setSpinVelocity(firstOrbitVel);
		secondOrbitCommand.setStopOnEnd(false);


		// Second Orbit -> Third Orbit
		double x1 = f2m(14.75), y1 = f2m(0.75);
		double x2 = f2m(19.75), y2 = f2m(-5);
		double pathAngle = Math.atan2(y2 - y1, x2 - x1);
		Pose2d secondOrbitToThirdOrbitStart = makePose(x1, y1, Math.toDegrees(pathAngle));
		Pose2d secondOrbitToThirdOrbitEnd = makePose(x2, y2, Math.toDegrees(pathAngle));
		RobotPath secondOrbitToThirdOrbitPath = new RobotPath(
				secondOrbitToThirdOrbitStart,
				secondOrbitToThirdOrbitEnd
		);
		secondOrbitToThirdOrbitPath.setStartVel(firstOrbitVel);
//		secondOrbitToThirdOrbitPath.setEndVelocity(firstOrbitVel);

		Trajectory secondOrbitToThirdOrbitTrajectory = secondOrbitToThirdOrbitPath.generateTrajectory();
		PathFollower secondOrbitToThirdOrbitFollower = new PathFollower();
		secondOrbitToThirdOrbitFollower.setTrajectory(secondOrbitToThirdOrbitTrajectory);
		secondOrbitToThirdOrbitFollower.setTol(0.3);
		PathFollowCommand secondOrbitToThirdOrbitCommand = new PathFollowCommand(secondOrbitToThirdOrbitFollower, true);

		Translation2d thirdOrbitOffset = makeTranslationF(0, 2.5);
		SimpleOrbitCommand thirdOrbitCommand = new SimpleOrbitCommand(thirdOrbitOffset, 190);
		thirdOrbitCommand.setSpinVelocity(firstOrbitVel);
		thirdOrbitCommand.setStopOnEnd(false);

		// Third Orbit -> End
		Pose2d thirdOrbitToEndStart = makePose(f2m(24.5), f2m(0), 180);
		Pose2d thirdOrbitToEndEnd = makePose(f2m(0), f2m(3.5 - .5), 180);
		RobotPath thirdOrbitToEndPath = new RobotPath(
				thirdOrbitToEndStart,
				thirdOrbitToEndEnd
		);
		thirdOrbitToEndPath.addCheckpoint(makeTranslationF(9, 4.5));
		thirdOrbitToEndPath.setStartVel(firstOrbitVel);

		Trajectory thirdOrbitToEndTrajectory = thirdOrbitToEndPath.generateTrajectory();
		PathFollower thirdOrbitToEndFollower = new PathFollower(1.8);
		thirdOrbitToEndFollower.setTrajectory(thirdOrbitToEndTrajectory/*, Rotation2d.fromDegrees(180)*/);
		PathFollowCommand thirdOrbitToEndCommand = new PathFollowCommand(thirdOrbitToEndFollower, true);


		// All together now
		var barrelCommand =
				startToFirstOrbitCommand.andThen(firstOrbitCommand)
						.andThen(firstOrbitToSecondOrbitCommand).andThen(secondOrbitCommand)
						.andThen(secondOrbitToThirdOrbitCommand).andThen(thirdOrbitCommand)
						.andThen(thirdOrbitToEndCommand);
		barrelCommand.schedule();
	}

	@Override
	public void run() {
		CommandScheduler.getInstance().run();
	}
}
