package frc.robot.autos;

import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.Drivetrain;
import frc.robot.PathFollower;
import frc.robot.RobotPath;
import frc.robot.autos.commands.PathFollowCommand;
import frc.robot.autos.commands.SimpleOrbitCommand;

import static frc.robot.utils.Handy.f2m;
import static frc.robot.utils.Handy.makePose;

public class Bounce extends Auto {
	@Override
	public void generate() {
		// Orbit -> First Objective
		Translation2d firstOrbitOffset = new Translation2d(f2m(1.5), f2m(2.));
		SimpleOrbitCommand firstOrbitCommand = new SimpleOrbitCommand(firstOrbitOffset, 115 /*yay*/);
		firstOrbitCommand.setStopOnEnd(true);
		firstOrbitCommand.setFieldRelative(false);
		// First Objective -> Orbit
		double x1Start = f2m(4), y1Start = f2m(2);
		double x2Start = f2m(7.5), y2Start = f2m(-3);
		double pathAngle = Math.toDegrees(Math.atan2(y2Start - y1Start, x2Start - x1Start));
		Pose2d firstObjectiveToSecondObjectiveStart = makePose(x1Start, y1Start, pathAngle);
		double x1End = f2m(12.5);
		double y1End = f2m(-3);
		double x2End = f2m(12.5);
		double y2End = f2m(2.7);
		pathAngle = Math.toDegrees(Math.atan2(y2End - y1End, x2End - x1End));
		Pose2d firstObjectiveToSecondObjectiveEnd = makePose(x2End, y2End, pathAngle);
		RobotPath firstObjectiveToSecondObjectivePath = new RobotPath(
				firstObjectiveToSecondObjectiveStart,
				firstObjectiveToSecondObjectiveEnd
		);
		firstObjectiveToSecondObjectivePath.addCheckpoint(new Translation2d(x2Start, y2Start));
		firstObjectiveToSecondObjectivePath.addCheckpoint(new Translation2d(f2m(9), f2m(-7)));
		firstObjectiveToSecondObjectivePath.addCheckpoint(new Translation2d(x1End, y1End));

		Trajectory firstObjectiveToSecondObjectiveTrajectory = firstObjectiveToSecondObjectivePath.generateTrajectory();
		PathFollower firstObjectiveToSecondObjectiveFollower = new PathFollower();
		firstObjectiveToSecondObjectiveFollower.setTrajectory(firstObjectiveToSecondObjectiveTrajectory, Rotation2d.fromDegrees(90));
		PathFollowCommand firstObjectiveToSecondObjectiveCommand = new PathFollowCommand(firstObjectiveToSecondObjectiveFollower);

		RobotPath secondObjectiveToThirdObjective = new RobotPath("Bounce2", 
		makePose(x2End, y2End, -90),
		makePose(f2m(21.5), f2m(4.25), 90)
		);

		secondObjectiveToThirdObjective.addCheckpoint(new Translation2d(x2End, f2m(-3)));
		secondObjectiveToThirdObjective.addCheckpoint(new Translation2d(f2m(17), f2m(-7)));
		secondObjectiveToThirdObjective.addCheckpoint(new Translation2d(f2m(20), f2m(-3)));
		var secondToThirdFollower = new PathFollower();
		secondToThirdFollower.setTrajectory(secondObjectiveToThirdObjective.generateTrajectory(), Rotation2d.fromDegrees(90));
		secondToThirdFollower.setTol(Units.feetToMeters(0.5));
		var secondObjectiveToThirdObjectiveCmd = new PathFollowCommand(secondToThirdFollower, true);

		Translation2d thirdToEndOffset = new Translation2d(f2m(-2.5), f2m(-.2));
		SimpleOrbitCommand thirdObjectiveOrbit = new SimpleOrbitCommand(thirdToEndOffset, 90);
		thirdObjectiveOrbit.setFieldRelative(true);
		thirdObjectiveOrbit.setStopOnEnd(true);
		var bounce =
				firstOrbitCommand
						.andThen(firstObjectiveToSecondObjectiveCommand)
						.andThen(secondObjectiveToThirdObjectiveCmd.withTimeout(10))
						.andThen(thirdObjectiveOrbit);
		bounce.schedule();
	}


	@Override
	public void run() {
		CommandScheduler.getInstance().run();
	}
}
