package frc.robot.autos;

public enum AutoMode {
	None(null, "None"),
	GalacticSearchA(new GalacticSearchA(), "Galactic Search A"), GalacticSearchB(new GalacticSearchB(), "Galactic Search B"),
	Slalom(new SlalomNew(), "Slalom"), Bounce(new Bounce(), "Bounce"), Barrel(new Barrel(), "Barrel"),
	Test1(new Test1(), "Test 1"), Test2(null, "Test 2");

	private final Auto a;
	private final String name;

	AutoMode(Auto a, String name) {
		this.a = a;
		this.name = name;
	}

	public Auto getAuto() {
		return this.a;
	}

	public String getName() {
		return this.name;
	}
}
