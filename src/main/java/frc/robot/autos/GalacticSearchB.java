package frc.robot.autos;

import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig;
import frc.robot.Drivetrain;
import frc.robot.PathFollower;
import frc.robot.RobotPath;
import frc.robot.autos.commands.DropIntakeCommand;
import frc.robot.autos.commands.PathFollowCommand;
import frc.robot.autos.commands.RunIntakeCommand;

import static frc.robot.utils.Handy.*;

import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;

public class GalacticSearchB extends Auto {

    @Override
    public void generate() {
        Pose2d startPose = makePose(0, 0, 0);

        RobotPath galaxyB = new RobotPath(startPose, makePose(f2m(30.5 + 5), f2m(-2.5), 0));

        galaxyB.addCheckpoint(new Translation2d(f2m(7.5), f2m(2.5)));
        galaxyB.addCheckpoint(new Translation2d(f2m(10.5), f2m(-3)));
        galaxyB.addCheckpoint(new Translation2d(f2m(17.5), f2m(2.5 + .25)));
//        TrajectoryConfig victorsMom = new TrajectoryConfig(Drivetrain)
        Trajectory galaxyTrajectory = galaxyB.generateTrajectory();
        PathFollower galaxyFollower = new PathFollower();
        galaxyFollower.setTrajectory(galaxyTrajectory);
        PathFollowCommand galaxyCommand = new PathFollowCommand(galaxyFollower);
        galaxyCommand.setInitPose(startPose);

        var galaxySearchB = new DropIntakeCommand()
                .andThen(galaxyCommand.alongWith(pulseIntake().andThen(pulseIntake()).andThen(pulseIntake())
                        .andThen(pulseIntake()).andThen(pulseIntake()).andThen(pulseIntake()).andThen(pulseIntake())));

        galaxySearchB.schedule();
    }

    @Override
    public void run() {
        CommandScheduler.getInstance().run();
    }

    public Command pulseIntake() {
        Command cmd = new RunIntakeCommand(-.75).withTimeout(.4).andThen(new RunIntakeCommand(0).withTimeout(.1));
        for(int i=0; i<3; i++) {
            cmd = cmd.andThen(new RunIntakeCommand(-0.75).withTimeout(0.4).andThen(new RunIntakeCommand(0)).withTimeout(0.1));
        }
        return cmd;
    }
}
