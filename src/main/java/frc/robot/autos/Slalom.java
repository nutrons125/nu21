package frc.robot.autos;

import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig;
import edu.wpi.first.wpilibj.trajectory.TrajectoryGenerator;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.PathFollower;
import frc.robot.RobotPath;
import frc.robot.autos.commands.DrivePathCommand;
import frc.robot.autos.commands.OrbitCommand;
import frc.robot.autos.commands.SimpleOrbitCommand;

import java.io.FileNotFoundException;
import java.util.List;

public class Slalom extends Auto {
	static final double MAX_VEL = Units.feetToMeters(15); // Feet per second
	static final double MAX_ACC = Units.feetToMeters(5); // Feet per second squared

	@Override
	public void generate() {
		TrajectoryConfig startToOrbitConfig = new TrajectoryConfig(MAX_VEL, MAX_ACC);
		startToOrbitConfig.setEndVelocity(0); // FixMe: Edit this accordingly

//		TrajectoryConfig orbitToEndConfig = new TrajectoryConfig(MAX_VEL, MAX_ACC);

		var startRot = new Rotation2d(0);
		RobotPath startPath = new RobotPath(
				"Robotrail",
				new Pose2d(new Translation2d(0, 0), startRot),
				new Pose2d(new Translation2d(Units.feetToMeters(18), Units.feetToMeters(3)), startRot));
		startPath.addCheckpoint(new Translation2d(Units.feetToMeters(3.25), Units.feetToMeters(0)));
		startPath.addCheckpoint(new Translation2d(Units.feetToMeters(3.5), Units.feetToMeters(3.0)));
		startPath.addCheckpoint(new Translation2d(Units.feetToMeters(5), Units.feetToMeters(5.5)));
		startPath.addCheckpoint(new Translation2d(Units.feetToMeters(17.5), Units.feetToMeters(5.5)));

		Trajectory startToOrbitRobotTrajectory = startPath.generateTrajectory();

/*
		Trajectory orbitToEndTrajectory = TrajectoryGenerator.generateTrajectory(
				new Pose2d(new Translation2d(0, 0), Rotation2d.fromDegrees(0)),
				List.of(),
				new Pose2d(new Translation2d(1, 0), Rotation2d.fromDegrees(0)),
				orbitToEndConfig
		);
*/

		Translation2d orbitObstacleTranslation = new Translation2d(Units.feetToMeters(2.5), Units.feetToMeters(0));

		DrivePathCommand startToOrbit = new DrivePathCommand(startToOrbitRobotTrajectory);
		SimpleOrbitCommand orbitAroundLastBall = new SimpleOrbitCommand(orbitObstacleTranslation, 270);
//		DrivePathCommand orbitToEnd = new DrivePathCommand(orbitToEndTrajectory);

		var slalom = new SequentialCommandGroup(
				startToOrbit,
				orbitAroundLastBall
//				orbitToEnd
		);

		slalom.schedule();
	}

	@Override
	public void run() {
		SmartDashboard.putBoolean("Slalom Running", true);
	}

	@Override
	public String getName() {
		return "Slalom";
	}
}
