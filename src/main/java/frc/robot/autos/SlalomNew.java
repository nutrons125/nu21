package frc.robot.autos;

import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Transform2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.Drivetrain;
import frc.robot.PathFollower;
import frc.robot.RobotPath;
import frc.robot.autos.commands.PathFollowCommand;
import frc.robot.autos.commands.SimpleOrbitCommand;
import frc.robot.utils.Handy;

public class SlalomNew extends Auto {
	boolean firstRun = true;
	Trajectory trajectory;
	PathFollower pathFollower;

	@Override
	public void generate() {
		final Translation2d orbitOffset = Handy.makeTranslationF(2.5, 0);
		final double orbitVelocity = Units.feetToMeters(8);
		final double orbitOmega = orbitVelocity / orbitOffset.getNorm();
		TrajectoryConfig config = new TrajectoryConfig(Drivetrain.MAX_MPS, Units.feetToMeters(15));
//			DrivePathCommand drivePathCommand = new DrivePathCommand(trajectory);
		var startRot = new Rotation2d(0);
		var path = new RobotPath(
				"Robotrail",
				new Pose2d(new Translation2d(0, 0), startRot),
				new Pose2d(new Translation2d(Units.feetToMeters(18.75), Units.feetToMeters(3)), startRot));

		path.addCheckpoint(new Translation2d(Units.feetToMeters(3.25), Units.feetToMeters(0)));
		path.addCheckpoint(new Translation2d(Units.feetToMeters(3.5), Units.feetToMeters(3.0)));
		path.addCheckpoint(new Translation2d(Units.feetToMeters(5), Units.feetToMeters(5.5)));
		path.addCheckpoint(new Translation2d(Units.feetToMeters(17), Units.feetToMeters(5.5)));
		path.setEndVelocity(orbitVelocity);
		path.setConfig(config);
		trajectory = path.generateTrajectory();
//			this.trajectory = RobotPath.fromFile("SlalomP1.wpilib.json");
		Transform2d firstState = new Transform2d(trajectory.getStates().get(0).poseMeters.getTranslation().unaryMinus(), new Rotation2d(0));
		trajectory = trajectory.transformBy(firstState);
		pathFollower = new PathFollower();
		pathFollower.setTrajectory(trajectory);
		PathFollowCommand slalomP1Command = new PathFollowCommand(pathFollower);
		SimpleOrbitCommand orbitCommand = new SimpleOrbitCommand(new Translation2d(Handy.f2m(2.5), 0), 350);

		orbitCommand.setSpinVelocity(orbitVelocity);

		var endOrbitPose = new Pose2d(new Translation2d(Units.feetToMeters(19), Units.feetToMeters(2)), startRot);

		/**
		 * WARNING!!!!!:
		 * 		DO NOT TOUCH ANYTHING!!!1!!
		 */
		RobotPath orbitToEndPath = new RobotPath("2",
				Handy.makePose(Units.feetToMeters(19), Units.feetToMeters(2), -90),
				Handy.makePose(Units.feetToMeters(0), Units.feetToMeters(12), 45));

//		orbitToEndPath.addCheckpoint(new Translation2d(Units.feetToMeters(19), Units.feetToMeters(0)));
		orbitToEndPath.addCheckpoint(new Translation2d(Units.feetToMeters(13), Units.feetToMeters(0)));
		orbitToEndPath.addCheckpoint(new Translation2d(Units.feetToMeters(6), Units.feetToMeters(0)));
		orbitToEndPath.addCheckpoint(new Translation2d(Units.feetToMeters(3.85), Units.feetToMeters(3))); // Was 3.75
		orbitToEndPath.addCheckpoint(new Translation2d(Units.feetToMeters(2.325), Units.feetToMeters(6)));
		orbitToEndPath.addCheckpoint(new Translation2d(Units.feetToMeters(0), Units.feetToMeters(8)));
		// orbitToEndPath.addCheckpoint(new Translation2d(Units.feetToMeters(3), Units.feetToMeters(5.3)));
		orbitToEndPath.setStartVel(orbitVelocity);
		orbitToEndPath.setConfig(config);
		//orbitToEndPath.addCheckpoint(new Translation2d(Units.feetToMeters(3), Units.feetToMeters(5.3)));
		var slalomPf = new PathFollower();
		slalomPf.setTrajectory(orbitToEndPath.generateTrajectory(), Rotation2d.fromDegrees(0));

		var totalCommand = slalomP1Command.andThen(orbitCommand).andThen(new PathFollowCommand(slalomPf, 10.8));
		totalCommand.schedule();
	}

	@Override
	public void run() {
//		if (firstRun) {
//			Drivetrain.getInstance().setPose(trajectory.getStates().get(0).poseMeters);
//			firstRun = false;
//		}
		CommandScheduler.getInstance().run();
		SmartDashboard.putBoolean("Slalom Running", true);
	}

	@Override
	public String getName() {
		return "Slalom";
	}
}
