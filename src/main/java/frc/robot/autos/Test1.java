package frc.robot.autos;

import edu.wpi.first.wpilibj.trajectory.Trajectory;
import frc.robot.Drivetrain;
import frc.robot.PathFollower;
import frc.robot.Robot;
import frc.robot.RobotPath;
import frc.robot.autos.Auto;

import java.io.FileNotFoundException;

public class Test1 extends Auto {
	boolean firstRun = true;
	Trajectory trajectory;
	PathFollower pathFollower;

	@Override
	public void generate() {
		try {
//			DrivePathCommand drivePathCommand = new DrivePathCommand(trajectory);
			this.trajectory = RobotPath.fromFile("5FootTranslate.wpilib.json");
			pathFollower = new PathFollower();
			pathFollower.setTrajectory(trajectory);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		if (firstRun) {
			Drivetrain.getInstance().setPose(trajectory.getStates().get(0).poseMeters);
			firstRun = false;
		}
		pathFollower.follow(Robot.secondsElapsed);
	}
}
