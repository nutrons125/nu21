package frc.robot.autos;

import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.util.Units;
import frc.robot.Drivetrain;
import frc.robot.Intake;
import frc.robot.PathFollower;
import frc.robot.RobotPath;
import frc.robot.autos.commands.DropIntakeCommand;
import frc.robot.autos.commands.PathFollowCommand;
import frc.robot.autos.commands.RunIntakeCommand;

import static frc.robot.utils.Handy.*;

import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;

public class GalacticSearchA extends Auto{

    @Override
    public void generate() {
        Pose2d startPose = makePose(0, Units.inchesToMeters(41), -18);

        RobotPath galaxyPath = new RobotPath(
            startPose, 
            makePose(f2m(30.5 + 5), f2m(13.5 - 7.5), 0)); //Fixme

        galaxyPath.addCheckpoint(new Translation2d(f2m(7.5), f2m(8 - 7.5)));
        Translation2d secondBallTranslation = new Translation2d(f2m(13.5), f2m(7 - 7.5));
        galaxyPath.addCheckpoint(secondBallTranslation);
        galaxyPath.addCheckpoint(new Translation2d(f2m(16 + 1.5), f2m(13 - 7.5)));
       
        Trajectory galaxyTrajectory = galaxyPath.generateTrajectory();
        Trajectory.State secondBallState = PathFollower.getStateClosestToPoint(galaxyTrajectory, secondBallTranslation);
        double secondBallTime = secondBallState.timeSeconds;

        PathFollower galaxyFollower = new PathFollower();
        galaxyFollower.setEndRotation(Rotation2d.fromDegrees(-20)); //Fixme
        galaxyFollower.setTrajectory(galaxyTrajectory);
        PathFollowCommand galaxyCommand = new PathFollowCommand(galaxyFollower);
        galaxyCommand.setSecondBallTime(.45);
        galaxyCommand.setInitPose(startPose);
        galaxyCommand.isGalaxyARed(true);



        var galaxySearchA = galaxyCommand.alongWith(new RunIntakeCommand(-1));
    
        //var itc = (new RuntIntake()).withTimeout(dafsd).andThen(stopIntake.withTimeout(sfdsdf)).andThen()


        // galaxySearchA.schedule();
        var cmdToRun = new DropIntakeCommand(-20).andThen(galaxySearchA);

//        var cmd = new DropIntakeCommand(-18);
        cmdToRun.schedule();
        // TODO RUN INTAKE FOREVER
    }


    @Override
    public void run() {
        Intake.getInstance().set(-1);
        CommandScheduler.getInstance().run();            
    }

    public Command pulseIntake() {
        return new RunIntakeCommand(-.75).withTimeout(.5).andThen(new RunIntakeCommand(0).withTimeout(.1));
    }
}