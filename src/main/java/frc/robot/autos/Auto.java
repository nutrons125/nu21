package frc.robot.autos;

public abstract class Auto {

	public void generate() {
	}

	public abstract void run();

	public String getName() {
		return this.getClass().getSimpleName();
	}
}
