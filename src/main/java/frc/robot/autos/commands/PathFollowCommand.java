package frc.robot.autos.commands;

import edu.wpi.first.wpilibj.controller.HolonomicDriveController;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Drivetrain;
import frc.robot.PathFollower;
import frc.robot.Robot;

import java.util.PriorityQueue;

public class PathFollowCommand extends CommandBase {
	private final PathFollower pathFollower;
	private final PriorityQueue<TimedAction> actions = new PriorityQueue<>();
	private double timeThreshold = 1.25;
	private double timeOffset = 0;
	private Pose2d resetPose = null;
	private HolonomicDriveController driveController;
	private boolean holdStartHeading = false;
	private Pose2d initPose;
	private boolean isGalaxyARed = false;

	public PathFollowCommand(PathFollower pf) {
		this.pathFollower = pf;
		driveController = Drivetrain.getInstance().getDriveController();
	}


	public PathFollowCommand(PathFollower pf, double headingP) {
		this(pf);
		this.driveController = Drivetrain.getInstance().getModifiedDriveController(headingP);
	}

	public PathFollowCommand(PathFollower pf, boolean holdStartHeading) {
		this(pf);
		this.holdStartHeading = holdStartHeading;
	}

	public void setSecondBallTime(double time) {
		timeThreshold = time;
	}

	public void isGalaxyARed(boolean isGalaxyARed) {
		this.isGalaxyARed = isGalaxyARed;
	}

	public void setInitPose(Pose2d initPose) {
		this.initPose = initPose;
	}

	@Override
	public void initialize() {
		timeOffset = Robot.secondsElapsed;
		pathFollower.setDriveController(driveController);

		if (initPose != null) {
			Drivetrain.getInstance().setPose(this.initPose);
		}

		if (holdStartHeading) {
			this.pathFollower.setEndRotation(Rotation2d.fromDegrees(-Drivetrain.getInstance().getHeading()));
		}
	}

	@Override
	public void execute() {
		double timeElapsed = Robot.secondsElapsed - timeOffset;
		if (isGalaxyARed) {
			if (timeElapsed > timeThreshold) {
				pathFollower.setEndRotation(Rotation2d.fromDegrees(90));
			}
		}
//		TimedAction currentAction = actions.peek();
//		while (currentAction != null && timeElapsed >= currentAction.timeTrigger) {
//			currentAction.action.run();
//			actions.poll();
//			currentAction = actions.peek();
//		}

		pathFollower.follow(timeElapsed);
	}

	public void addTimedAction(Runnable action, double timeTrigger) {
		TimedAction newAction = new TimedAction();
		newAction.action = action;
		newAction.timeTrigger = timeTrigger;
		actions.add(newAction);
	}


	@Override
	public void end(boolean inter) {
		Drivetrain.getInstance().drive(new ChassisSpeeds(0, 0, 0));
		if (resetPose != null) {
			var actualReset = new Pose2d(resetPose.getTranslation(), Drivetrain.getInstance().getCurrentPose().getRotation());
			Drivetrain.getInstance().setPose(actualReset);
		}
	}

	@Override
	public boolean isFinished() {
		return pathFollower.done();
	}

	public static class TimedAction implements Comparable<TimedAction> {
		public Runnable action = () -> {
		};
		public double timeTrigger = 0;

		@Override
		public int compareTo(TimedAction arg0) {
			if (this.timeTrigger > arg0.timeTrigger) {
				return 1;
			} else if (this.timeTrigger == arg0.timeTrigger) {
				return 0;
			} else {
				return -1;
			}
		}
	}
}
