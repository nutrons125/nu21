package frc.robot.autos.commands;

import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Drivetrain;

public class OrbitCommand extends CommandBase {
	private final ChassisSpeeds speeds;
	private Translation2d centerOfRotation;
	private double degreesToSpin;
	private Rotation2d targetAngle;

	public OrbitCommand(ChassisSpeeds speeds, Translation2d centerOfRotation, double degreesToSpin) {
		this.centerOfRotation = centerOfRotation;
		this.speeds = speeds;
		this.degreesToSpin = degreesToSpin;
		this.targetAngle = Rotation2d.fromDegrees(-Drivetrain.getInstance().getHeading()).plus(Rotation2d.fromDegrees(degreesToSpin));
	}

	public OrbitCommand(Trajectory previousTrajectory, Translation2d centerOfRotation, double degreesToSpin) {
		Trajectory.State previousState = previousTrajectory.sample(previousTrajectory.getTotalTimeSeconds());
		this.centerOfRotation = centerOfRotation;
		this.degreesToSpin = degreesToSpin;
		this.targetAngle = Rotation2d.fromDegrees(-Drivetrain.getInstance().getHeading()).plus(Rotation2d.fromDegrees(degreesToSpin));
		this.speeds = Drivetrain.getInstance().getDriveController().calculate(
				Drivetrain.getInstance().getCurrentPose(),
				previousState,
				targetAngle
		);
	}

	@Override
	public void initialize() {
	}

	@Override
	public void execute() {
		Drivetrain.getInstance().driveAboutCoR(speeds, centerOfRotation);
	}

	@Override
	public boolean isFinished() {
		// Finished when the degree error is less than 5
		return Math.abs(targetAngle.minus(Rotation2d.fromDegrees(-Drivetrain.getInstance().getHeading())).getDegrees()) < 5;
	}
}
