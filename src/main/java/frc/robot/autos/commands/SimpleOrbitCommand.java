package frc.robot.autos.commands;

import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Drivetrain;
import frc.robot.utils.Handy;

public class SimpleOrbitCommand extends CommandBase {
	private Translation2d cor;
	private final double degreesToSpin;
	private double initialRawHeading = 0;
	private Translation2d ballRelative;
	private double customOmega = Math.toRadians(180);
	private boolean stopOnEnd = false;
	private boolean firstExecute = false;
	private boolean fieldRelative = false;

	public SimpleOrbitCommand(Translation2d centerOfRotation, double degreesToSpin) {
		this.cor = centerOfRotation;
		this.degreesToSpin = degreesToSpin;
		customOmega *= Math.signum(degreesToSpin);
	}

	public SimpleOrbitCommand setSpinVelocity(double velocity) {
		this.customOmega = (velocity / cor.getNorm()) * Math.signum(degreesToSpin);
		return this;
	}

	public void setStopOnEnd(boolean stop) {
		this.stopOnEnd = stop;
	}

	public void setFieldRelative(boolean fieldRelative) {
		this.fieldRelative = fieldRelative;
	}

	@Override
	public void initialize() {
		if(fieldRelative) {
			cor = cor.rotateBy(Rotation2d.fromDegrees(-Drivetrain.getInstance().getHeading()));
		}

		this.initialRawHeading = Drivetrain.getInstance().getRawHeading();
	}

	@Override
	public void execute() {
		if (!firstExecute) {
			this.initialRawHeading = Drivetrain.getInstance().getRawHeading();
			firstExecute = true;
		}
		SmartDashboard.putNumber("StartingHeading", initialRawHeading);
		Drivetrain.getInstance().driveAboutCoR(new ChassisSpeeds(0, 0, customOmega), this.cor);
	}

	@Override
	public boolean isFinished() {
		double distanceTraveled = Drivetrain.getInstance().getRawHeading() - initialRawHeading;
		SmartDashboard.putNumber("Distance Traveled (orbit)", distanceTraveled);
		SmartDashboard.putNumber("Deg2Spin (orbit)", degreesToSpin);
		SmartDashboard.putString("Radius", cor.toString());
		SmartDashboard.putNumber("StartingHeading", initialRawHeading);
		return Math.abs(distanceTraveled) > Math.abs(degreesToSpin);
	}

	@Override
	public void end(boolean interrupted) {
		if (stopOnEnd) {
			Drivetrain.getInstance().drive(new ChassisSpeeds(0, 0, 0));
		}
	}
}
