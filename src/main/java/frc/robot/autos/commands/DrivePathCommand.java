package frc.robot.autos.commands;

import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.controller.ProfiledPIDController;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrapezoidProfile;
import edu.wpi.first.wpilibj2.command.SwerveControllerCommand;
import frc.robot.AutoRunner;
import frc.robot.Drivetrain;

public class DrivePathCommand extends SwerveControllerCommand {
	private static final TrapezoidProfile.Constraints constraints = new TrapezoidProfile.Constraints(1, 1);
	private static final ProfiledPIDController thetaControl = new ProfiledPIDController(0.9, 0, 0, constraints);

	static {
		thetaControl.enableContinuousInput(-Math.PI, Math.PI);
	}

	public DrivePathCommand(Trajectory trajectory) {
		super(trajectory,
				Drivetrain.getInstance()::getCurrentPose,
				Drivetrain.getInstance().getKinematics(),
				new PIDController(0.001, 0, 0),
				new PIDController(0.001, 0, 0),
				thetaControl,
				AutoRunner::getDesiredRotation,
				Drivetrain.getInstance()::driveStates,
				Drivetrain.getInstance()
		);
	}
}
