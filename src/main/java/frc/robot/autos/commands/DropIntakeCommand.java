package frc.robot.autos.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Drivetrain;
import frc.robot.Intake;

public class DropIntakeCommand extends CommandBase {

	public DropIntakeCommand() {

	}

	public DropIntakeCommand(double gyroOffset) {
		Drivetrain.getInstance().zeroGyro(gyroOffset);
	}

	@Override
	public void initialize() {
		Intake.getInstance().setServo(0);
	}

	@Override
	public boolean isFinished() {
		return true;
	}
}

