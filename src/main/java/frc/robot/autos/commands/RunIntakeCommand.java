package frc.robot.autos.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Intake;
import frc.robot.autos.Auto;

public class RunIntakeCommand extends CommandBase {
    private double power;

    public RunIntakeCommand(double power) {
        this.power = power;
    }

    @Override
    public void initialize() {
        Intake.getInstance().set(power);
    }

    @Override
    public void execute() {
        Intake.getInstance().set(power);
    }


    @Override
    public boolean isFinished() {
        return false;
    }
    @Override
    public void end(boolean interrupted) {
       //var itc = (new RuntIntake()).withTimeout(dafsd).andThen(stopIntake.withTimeout(sfdsdf)).andThen()
    }
}
