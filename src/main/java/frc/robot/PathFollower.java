package frc.robot;

import edu.wpi.first.wpilibj.controller.HolonomicDriveController;
import edu.wpi.first.wpilibj.controller.ProfiledPIDController;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.kinematics.SwerveDriveOdometry;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrapezoidProfile;
import edu.wpi.first.wpilibj.util.Units;
import frc.robot.utils.Handy;

// Wheelbase width : 0.539496 meters
public class PathFollower {
	private SwerveDriveOdometry swerveOdometry;
	private Trajectory trajectory;
	private Field2d field2d = new Field2d();
	private Rotation2d endRot = new Rotation2d();
	private HolonomicDriveController driveController;
	private double tol = Units.feetToMeters(0.3);

	public PathFollower() {
		this(0.9);
	}

	public PathFollower(double headingP) {
		TrapezoidProfile.Constraints constraints = new TrapezoidProfile.Constraints(Drivetrain.MAX_MPS, Drivetrain.MAX_ACCEL);

		var thetaControl = new ProfiledPIDController(headingP, 0, 0, constraints);
		thetaControl.enableContinuousInput(-Math.PI, Math.PI);

		swerveOdometry = new SwerveDriveOdometry(Drivetrain.getInstance().getKinematics(),
				Rotation2d.fromDegrees(-Drivetrain.getInstance().getHeading()));

		SmartDashboard.putData("Path simulation", field2d);
		driveController = Drivetrain.getInstance().getDriveController();
	}

	public static Pose2d makePose(double xMeters, double yMeters, double thetaDeg) {
		return new Pose2d(xMeters, yMeters, Rotation2d.fromDegrees(thetaDeg));
	}

	public static Trajectory.State getStateClosestToPoint(Trajectory trajectory, Translation2d point) {
		double minDistance = Double.MAX_VALUE;
		Trajectory.State minState = null;
		for (Trajectory.State state : trajectory.getStates()) {
			double distanceToPoint = state.poseMeters.getTranslation().getDistance(point);
			if (distanceToPoint < minDistance) {
				minState = state;
				minDistance = distanceToPoint;
			}
		}
		return minState;
	}

	public void addAction(Runnable act) {

	}

	public void setDriveController(HolonomicDriveController dc) {
		this.driveController = dc;
	}

	public void setTrajectory(Trajectory trajectory, Rotation2d endRot) {
		this.endRot = endRot;
		setTrajectory(trajectory);
	}

	public void setTrajectory(Trajectory trajectory) {
		this.trajectory = trajectory;
	}

	public void setEndRotation(Rotation2d endRotation) {
		this.endRot = endRotation;
	}

	public void follow(double time) {
		Trajectory.State stateRef = trajectory.sample(time);
//		if (time > trajectory.getTotalTimeSeconds()) {
//			Drivetrain.getInstance().driveVelocity(new ChassisSpeeds(0, 0, 0));
//			return true;
//		}

		Pose2d currentPose = Drivetrain.getInstance().getCurrentPose();
		SmartDashboard.putString("Reference Pose", Handy.poseMtoPoseF(stateRef.poseMeters).toString());
		SmartDashboard.putNumber("Pose Error X", stateRef.poseMeters.minus(currentPose).getX());
		SmartDashboard.putNumber("Pose Error Y", stateRef.poseMeters.minus(currentPose).getY());
		SmartDashboard.putNumber("Pose Error R", endRot.minus(currentPose.getRotation()).getDegrees());
		SmartDashboard.putNumber("Rotation Setpoint", endRot.getDegrees());
		field2d.setRobotPose(stateRef.poseMeters);
		ChassisSpeeds outputSpeeds = driveController.calculate(currentPose, stateRef.poseMeters, stateRef.velocityMetersPerSecond, endRot);
		Drivetrain.getInstance().drive(outputSpeeds);
		SmartDashboard.putString("Path output", outputSpeeds.toString());
	}

	private void setEndRot(Rotation2d endRot) {
		this.endRot = endRot;
	}

	public Pose2d getPosition() {
		return swerveOdometry.getPoseMeters();
	}

	public void setTol(double t) {
		this.tol = t;
	}

	/**
	 * FixMe: endPose.minus(currentPose).getTranslation().getNorm() < 0.5;
	 *
	 * @return
	 */
	public boolean done() {
		Pose2d currentPose = Drivetrain.getInstance().getCurrentPose();
		Pose2d endPose = trajectory.sample(trajectory.getTotalTimeSeconds()).poseMeters;

		return endPose.minus(currentPose).getTranslation().getNorm() < tol;
	}
}
