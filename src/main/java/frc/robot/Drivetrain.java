package frc.robot;

import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.controller.HolonomicDriveController;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.controller.ProfiledPIDController;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.kinematics.SwerveDriveKinematics;
import edu.wpi.first.wpilibj.kinematics.SwerveDriveOdometry;
import edu.wpi.first.wpilibj.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.trajectory.TrapezoidProfile;
import edu.wpi.first.wpilibj.trajectory.constraint.SwerveDriveKinematicsConstraint;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.utils.Handy;
import frc.robot.utils.Rotation2dPIDController;

import static frc.robot.Drivetrain.DriveConstants.*;

/**
 * Drivetraini
 */
public class Drivetrain extends SubsystemBase {
	public static final double WHEELBASE_WIDTH_M = 0.539496;
	public static final double WHEELBASE_WIDTH_F = Handy.m2f(0.539496);
	public static final double BUMPER_WIDTH = 0.08255;

	public static final double MAX_MPS = Units.feetToMeters(15); // Max meters per second
	public static final double MAX_RPS = Units.degreesToRadians(180); // Max radians per second
	public static final double MAX_ACCEL = Units.feetToMeters(15); // Max accel per second squared

	private static Drivetrain instance;
	private final SwerveDriveKinematics swerveKinematics = new SwerveDriveKinematics(
			new Translation2d(WHEELBASE_WIDTH_M / 2, WHEELBASE_WIDTH_M / 2),
			new Translation2d(WHEELBASE_WIDTH_M / 2, -WHEELBASE_WIDTH_M / 2),
			new Translation2d(-WHEELBASE_WIDTH_M / 2, WHEELBASE_WIDTH_M / 2),
			new Translation2d(-WHEELBASE_WIDTH_M / 2, -WHEELBASE_WIDTH_M / 2));
	private final SwerveDriveKinematicsConstraint swerveConstraint = new SwerveDriveKinematicsConstraint(swerveKinematics, MAX_MPS);
	private final SwerveDriveOdometry swerveOdometry;
	private final HolonomicDriveController driveController;
	private final AHRS imu;
	private final SwerveModule frontLeftModule, frontRightModule, backLeftModule, backRightModule;
	private final Rotation2dPIDController headingCtrl = new Rotation2dPIDController(.02, 0, 0);
	double gyroOffset = 0;
	private ChassisSpeeds inputSpeeds = new ChassisSpeeds();
	private OutputMode outputMode = OutputMode.PERCENT;
	private boolean fieldOriented = false;

	/**
	 * Constructor
	 */
	private Drivetrain() {
		imu = new AHRS(I2C.Port.kMXP);

		frontLeftModule = new SwerveModule(FRONT_LEFT_ROTATE_ID, FRONT_LEFT_THROTTLE_ID, FRONT_LEFT_ENCODER_ID, true, Modules.FRONT_LEFT);
		frontRightModule = new SwerveModule(FRONT_RIGHT_ROTATE_ID, FRONT_RIGHT_THROTTLE_ID, FRONT_RIGHT_ENCODER_ID, true, Modules.FRONT_RIGHT);
		backLeftModule = new SwerveModule(BACK_LEFT_ROTATE_ID, BACK_LEFT_THROTTLE_ID, BACK_LEFT_ENCODER_ID, false, Modules.BACK_LEFT);
		backRightModule = new SwerveModule(BACK_RIGHT_ROTATE_ID, BACK_RIGHT_THROTTLE_ID, BACK_RIGHT_ENCODER_ID, true, Modules.BACK_RIGHT);

		swerveOdometry = new SwerveDriveOdometry(swerveKinematics, new Rotation2d());

		TrapezoidProfile.Constraints constraints = new TrapezoidProfile.Constraints(Math.PI, Math.PI);
		var thetaControl = new ProfiledPIDController(4, 0, 0, constraints);
		thetaControl.enableContinuousInput(-Math.PI, Math.PI);

		driveController = new HolonomicDriveController(
				new PIDController(1., 0, 0),
				new PIDController(1., 0, 0),
				thetaControl);
	}

	/**
	 * @return the instance
	 */
	public static Drivetrain getInstance() {
		return instance = instance == null ? new Drivetrain() : instance;
	}

	public void driveManual(double throttle, double spin) {
		frontLeftModule.setManual(throttle, spin);
		frontRightModule.setManual(throttle, spin);
		backLeftModule.setManual(throttle, spin);
		backRightModule.setManual(throttle, spin);
	}

	public void drive(ChassisSpeeds speeds) {
		switch (this.outputMode) {
			case PERCENT:
				drivePercent(speeds);
				break;
			case VELOCITY:
				driveVelocity(speeds);
		}
	}

	public void driveHoldHeading(ChassisSpeeds speeds, double headingToHold) {
		switch (this.outputMode) {
			case PERCENT:
				driveHoldHeadingPercent(speeds, headingToHold);
				break;
			case VELOCITY:
				driveHoldHeadingVelocity(speeds, headingToHold);
		}
	}

	private void drivePercent(ChassisSpeeds speeds) {
		this.inputSpeeds = speeds;
		SwerveModuleState[] states = swerveKinematics.toSwerveModuleStates(speeds);
		frontLeftModule.setPercent(states[0]);
		frontRightModule.setPercent(states[1]);
		backLeftModule.setPercent(states[2]);
		backRightModule.setPercent(states[3]);
	}

	private void driveVelocity(ChassisSpeeds speeds) {
		this.inputSpeeds = speeds;
		SwerveModuleState[] states = swerveKinematics.toSwerveModuleStates(speeds);
		frontLeftModule.setVelocity(states[0]);
		frontRightModule.setVelocity(states[1]);
		backLeftModule.setVelocity(states[2]);
		backRightModule.setVelocity(states[3]);
	}

	public void driveAboutCoR(ChassisSpeeds speeds, Translation2d corOffset) {
		SwerveModuleState[] states = swerveKinematics.toSwerveModuleStates(speeds, corOffset);
		frontLeftModule.setVelocity(states[0]);
		frontRightModule.setVelocity(states[1]);
		backLeftModule.setVelocity(states[2]);
		backRightModule.setVelocity(states[3]);
	}

	public void driveStates(SwerveModuleState[] states) {
		frontLeftModule.setVelocity(states[0]);
		frontRightModule.setVelocity(states[1]);
		backLeftModule.setVelocity(states[2]);
		backRightModule.setVelocity(states[3]);
	}

	private void driveHoldHeadingVelocity(ChassisSpeeds speeds, double desiredHeading) {
		speeds.omegaRadiansPerSecond = headingCtrl.calculate(-getHeading(), desiredHeading);
		driveVelocity(speeds);
	}

	private void driveHoldHeadingPercent(ChassisSpeeds speeds, double desiredHeading) {
		speeds.omegaRadiansPerSecond = headingCtrl.calculate(-getHeading(), desiredHeading);
		drivePercent(speeds);
	}

	/**
	 * Returns the heading from the AHRS
	 */
	public double getHeading() {
		return Rotation2d.fromDegrees(imu.getAngle()).rotateBy(Rotation2d.fromDegrees(0)).getDegrees() + gyroOffset;
	}

	public double getRawHeading() {
		return -imu.getAngle();
	}

	SwerveModuleState[] getModuleStates() {
		SwerveModuleState frontLeftState = frontLeftModule.getModuleState();
		SwerveModuleState frontRightState = frontRightModule.getModuleState();
		SwerveModuleState backLeftState = backLeftModule.getModuleState();
		SwerveModuleState backRightState = backRightModule.getModuleState();

		return new SwerveModuleState[]{
				frontLeftState,
				frontRightState,
				backLeftState,
				backRightState
		};
	}

	public SwerveDriveKinematics getKinematics() {
		return swerveKinematics;
	}

	public SwerveDriveKinematicsConstraint getConstraint() {
		return swerveConstraint;
	}

	public HolonomicDriveController getDriveController() {
		return driveController;
	}

	public HolonomicDriveController getModifiedDriveController(double headingP) {
		TrapezoidProfile.Constraints constraints = new TrapezoidProfile.Constraints(Math.PI, Math.PI);
		var thetaControl = new ProfiledPIDController(2, 0, 0, constraints);
		thetaControl.enableContinuousInput(-Math.PI, Math.PI);

		return new HolonomicDriveController(new PIDController(1, 0, 0),
				new PIDController(1, 0, 0),
				thetaControl);
	}

	public Pose2d getCurrentPose() {
		return swerveOdometry.getPoseMeters();
	}

	public boolean isFieldOriented() {
		return fieldOriented;
	}

	public void setFieldOriented(boolean fieldOriented) {
		this.fieldOriented = fieldOriented;
	}

	public OutputMode getOutputMode() {
		return outputMode;
	}

	public void setOutputMode(OutputMode mode) {
		this.outputMode = mode;
	}

	public void setPose(Pose2d pose) {
		swerveOdometry.resetPosition(pose, Rotation2d.fromDegrees(-getHeading()));
	}

	/**
	 * Makes the modules move
	 */
	private void updateModules() {
		frontLeftModule.update();
		frontRightModule.update();
		backLeftModule.update();
		backRightModule.update();
	}

	public void update() {
		updateOdometry();
		updateModules();
		updateShuffleboard();
	}

	private void updateOdometry() {
		swerveOdometry.update(Rotation2d.fromDegrees(-getHeading()), getModuleStates());
	}

	/**
	 * Updates Labview dash
	 */
	public void updateShuffleboard() {
		frontLeftModule.updateSmart();
		frontRightModule.updateSmart();
		backLeftModule.updateSmart();
		backRightModule.updateSmart();

		double x = Units.metersToFeet(getCurrentPose().getX());
		double y = Units.metersToFeet(getCurrentPose().getY());
		double t = getCurrentPose().getRotation().getDegrees();

		SmartDashboard.putString("Pose", String.format("X: %.2f Y: %.2f T:%.2f", x, y, t));
		SmartDashboard.putNumber("Velocity X", inputSpeeds.vxMetersPerSecond);
		SmartDashboard.putNumber("Velocity Y", inputSpeeds.vyMetersPerSecond);
		SmartDashboard.putNumber("Velocity Omega", Math.toDegrees(inputSpeeds.omegaRadiansPerSecond));
		SmartDashboard.putNumber("IMU Heading", getHeading());
		SmartDashboard.putNumber("Raw Heading", getRawHeading());
		SmartDashboard.putNumber("Rot ctrl Error", headingCtrl.getPositionError());
		SmartDashboard.putBoolean("Field-Oriented Enabled", this.fieldOriented);
	}

	public void resetPose() {
		swerveOdometry.resetPosition(new Pose2d(), new Rotation2d());
	}

	public void zeroEncoders() {
		frontLeftModule.zeroEncoder();
		frontRightModule.zeroEncoder();
		backLeftModule.zeroEncoder();
		backRightModule.zeroEncoder();
	}

	public void zeroGyro() {
		zeroGyro(0);
	}

	// RED A Offset = -18 degrees
	public void zeroGyro(double gyroOffset) {
		this.gyroOffset = gyroOffset;
		imu.zeroYaw();
	}

	/**
	 * Why is this abstract
	 */
	public void initDefaultCommand() {
	}

	public void pointZero() {
		frontLeftModule.pointZero();
		frontRightModule.pointZero();
		backLeftModule.pointZero();
		backRightModule.pointZero();
	}

	enum OutputMode {
		PERCENT, VELOCITY
	}

	enum Modules {
		FRONT_LEFT, FRONT_RIGHT, BACK_RIGHT, BACK_LEFT
	}

	/**
	 * Constants for the drivetrain
	 */
	static final class DriveConstants {
		// Hardware IDs
		static final short FRONT_LEFT_ROTATE_ID = 27;
		static final short FRONT_LEFT_THROTTLE_ID = 11;
		static final short FRONT_LEFT_ENCODER_ID = 3;

		static final short FRONT_RIGHT_ROTATE_ID = 22;
		static final short FRONT_RIGHT_THROTTLE_ID = 43;
		static final short FRONT_RIGHT_ENCODER_ID = 1;

		static final short BACK_LEFT_ROTATE_ID = 23;
		static final short BACK_LEFT_THROTTLE_ID = 31;
		static final short BACK_LEFT_ENCODER_ID = 0;

		static final short BACK_RIGHT_ROTATE_ID = 21;
		static final short BACK_RIGHT_THROTTLE_ID = 42;
		static final short BACK_RIGHT_ENCODER_ID = 2;


		// Driver preferences
		static final double THROTTLE_DEADBAND = 0.1;
		static final double THROTTLE_MAX = 0.5;
		static final double ROTATE_DEADBAND = 0.1;
		static final double ROTATE_MAX = 0.5;
	}
}
