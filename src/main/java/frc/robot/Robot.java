// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.autos.AutoMode;
import frc.robot.utils.Controller;

public class Robot extends TimedRobot {
	/*
		 static final double TRANSLATE_SCALE = .3,
				 ROTATE_SCALE = .3,
				 VEL_TRANSLATE_SCALE = .5,
				 VEL_ROTATE_SCALE = .45;
	*/
	static final double TRANSLATE_SCALE = 1,
			ROTATE_SCALE = 1,
			VEL_TRANSLATE_SCALE = 1,
			VEL_ROTATE_SCALE = 1;
	public static double secondsElapsed = 0;
	Controller driverCtrl;
	Drivetrain drivetrain;
	Intake intake;
	SendableChooser<Integer> driveModeChooser, outputModeChooser;
	SendableChooser<AutoMode> autoChooser;
	RobotPath path;
	PathFollower pathFollower = new PathFollower();
	AutoRunner autoRunner;
	double initTime = 0;
	double headingToHold = 0;
	boolean pathFinished = false;


	{
		var startRot = new Rotation2d(0);
		path = new RobotPath(
				"Robotrail",
				new Pose2d(new Translation2d(0, 0), startRot),
				new Pose2d(new Translation2d(Units.feetToMeters(0), Units.feetToMeters(5.5)), startRot));

		path.addCheckpoint(new Translation2d(Units.feetToMeters(3.25), Units.feetToMeters(0)));
		path.addCheckpoint(new Translation2d(Units.feetToMeters(3.5), Units.feetToMeters(3.0)));
		path.addCheckpoint(new Translation2d(Units.feetToMeters(5), Units.feetToMeters(5.5)));

		path.addCheckpoint(new Translation2d(Units.feetToMeters(17), Units.feetToMeters(5.5)));
		path.addCheckpoint(new Translation2d(Units.feetToMeters(19), Units.feetToMeters(0.5)));

		path.addCheckpoint(new Translation2d(Units.feetToMeters(24), Units.feetToMeters(0.5)));
		path.addCheckpoint(new Translation2d(Units.feetToMeters(23.75), Units.feetToMeters(5.5)));
		path.addCheckpoint(new Translation2d(Units.feetToMeters(19), Units.feetToMeters(5.5)));
		path.addCheckpoint(new Translation2d(Units.feetToMeters(18.5), Units.feetToMeters(0.5)));

		path.addCheckpoint(new Translation2d(Units.feetToMeters(4), Units.feetToMeters(0.5)));
		path.addCheckpoint(new Translation2d(Units.feetToMeters(3.5), Units.feetToMeters(5.5)));


		// path.addCheckpoint(new Translation2d(Units.feetToMeters(5), Units.feetToMeters(5.5)));
		// path.addCheckpoint(new Translation2d(Units.feetToMeters(10), Units.feetToMeters(5.5)));
		// path.addCheckpoint(new Translation2d(Units.feetToMeters(15), Units.feetToMeters(5)));
		// path.addCheckpoint(new Translation2d(Units.feetToMeters(20), Units.feetToMeters(2.5)));
		// path.addCheckpoint(new Translation2d(Units.feetToMeters(21), Units.feetToMeters(-1)));
		// path.addCheckpoint(new Translation2d(Units.feetToMeters(22.5), Units.feetToMeters(2.5)));
		// path.addCheckpoint(new Translation2d(Units.feetToMeters(20), Units.feetToMeters(5)));
		// path.addCheckpoint(new Translation2d(Units.feetToMeters(20), Units.feetToMeters(2.5)));
		// path.addCheckpoint(new Translation2d(Units.feetToMeters(15), Units.feetToMeters(0)));
		// path.addCheckpoint(new Translation2d(Units.feetToMeters(10), Units.feetToMeters(-1)));
		// path.addCheckpoint(new Translation2d(Units.feetToMeters(5), Units.feetToMeters(0)));
		// path.addCheckpoint(new Translation2d(Units.feetToMeters(2.5), Units.feetToMeters(2.5)));
		// path.addCheckpoint(new Translation2d(Units.feetToMeters(0), Units.feetToMeters(5)));


		pathFollower.setTrajectory(path.generateTrajectory(), Rotation2d.fromDegrees(0));


//		try {
//			pathFollower.setTrajectory(PathFollower.RobotPath.fromFile("D1Start_Blue.wpilib.json"));
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//
	}

	/**
	 * Scales a joystick input to a step function
	 *
	 * @param joystickValue Joystick value
	 * @param minPower      Min power needed to move robot
	 * @param bX            Joystick value at start of the second step
	 * @param bY            Output value at start of the second step
	 * @return Scaled joystick power
	 */
	public static double scaleJoystickInput(double joystickValue, double minPower, double bX, double bY) {
		if (Math.abs(joystickValue) > bX) {
			return ((Math.abs(joystickValue) - bX) * ((1 - bY) / (1 - bX)) + bY) * Math.signum(joystickValue);
		} else if (Math.abs(joystickValue) > 0) {
			return (Math.abs(joystickValue) * ((bY - minPower) / bX) + minPower) * Math.signum(joystickValue);
		}
		return 0.0;
	}

	/**
	 * Reduces turning power as the robot goes faster
	 *
	 * @param thetaPower    Turning power
	 * @param xPower        X axis power
	 * @param yPower        Y axis power
	 * @param kTheta        Constant (0-1)
	 * @param minThetaPower Min turning power
	 * @return Scaled turning power
	 */
	public static double scaleTurningPower(double thetaPower, double xPower, double yPower, double kTheta, double minThetaPower) {
		double xyPower = Math.min(Math.sqrt(xPower * xPower + yPower * yPower), 1);
		return Math.max(Math.abs(thetaPower) * (1 - (xyPower * kTheta)), minThetaPower) * Math.signum(thetaPower);
	}

	@Override
	public void robotInit() {
		intake = Intake.getInstance();
		driverCtrl = new Controller(0);
		drivetrain = Drivetrain.getInstance();

		driveModeChooser = new SendableChooser<>();
		outputModeChooser = new SendableChooser<>();
		autoChooser = new SendableChooser<>();

		driveModeChooser.setDefaultOption("Field-Oriented", 0);
		driveModeChooser.addOption("Relative", 0);
		driveModeChooser.addOption("Field-Oriented", 1);

		outputModeChooser.setDefaultOption("Velocity", 0);
		outputModeChooser.addOption("Percent", 0);
		outputModeChooser.addOption("Velocity", 1);

		autoChooser.setDefaultOption(AutoMode.None.getName(), AutoMode.None);
		autoChooser.addOption(AutoMode.GalacticSearchA.getName(), AutoMode.GalacticSearchA);
		autoChooser.addOption(AutoMode.GalacticSearchB.getName(), AutoMode.GalacticSearchB);
		autoChooser.addOption(AutoMode.Slalom.getName(), AutoMode.Slalom);
		autoChooser.addOption(AutoMode.Bounce.getName(), AutoMode.Bounce);
		autoChooser.addOption(AutoMode.Barrel.getName(), AutoMode.Barrel);
		autoChooser.addOption(AutoMode.Test1.getName(), AutoMode.Test1);
		autoChooser.addOption(AutoMode.Test2.getName(), AutoMode.Test2);

		SmartDashboard.putData(driveModeChooser);
		SmartDashboard.putData(outputModeChooser);
		SmartDashboard.putData(autoChooser);

		drivetrain.setFieldOriented(true);
		drivetrain.zeroGyro();

		autoRunner = new AutoRunner();

		drivetrain.zeroGyro();
	}

	@Override
	public void robotPeriodic() {
		drivetrain.updateShuffleboard();
		SmartDashboard.putNumber("Left Stick Magnitude", driverCtrl.getLeftStickMagnitude());
		SmartDashboard.putNumber("Left Stick direction", driverCtrl.getLeftStickDirection());
		SmartDashboard.putNumber("Right Stick direction", driverCtrl.getRightStickDirection());
		SmartDashboard.putBoolean("A Pressed", driverCtrl.aPressed());
		SmartDashboard.putNumber("headingToHold", headingToHold);
		SmartDashboard.putBoolean("Path Finished", pathFinished);
		driverCtrl.updateShuffleboard();
	}

	@Override
	public void autonomousInit() {
		autoRunner.setAuto(autoChooser.getSelected());
		drivetrain.zeroGyro();
		drivetrain.resetPose();
		drivetrain.setOutputMode(Drivetrain.OutputMode.VELOCITY);
//		drivetrain.setFieldOriented(true);
		initTime = Timer.getFPGATimestamp();
		CommandScheduler.getInstance().onCommandExecute((cmd) -> SmartDashboard.putString("Command", cmd.getName()));
	}

	@Override
	public void autonomousPeriodic() {
		secondsElapsed = Timer.getFPGATimestamp() - initTime;
		drivetrain.update();
//		this.pathFinished = pathFollower.follow(Timer.getFPGATimestamp() - initTime);
		autoRunner.run();
	}

	@Override
	public void teleopInit() {
		drivetrain.setFieldOriented(driveModeChooser.getSelected() == 1);
		drivetrain.setOutputMode(outputModeChooser.getSelected() == 0 ? Drivetrain.OutputMode.PERCENT : Drivetrain.OutputMode.VELOCITY);
		driverCtrl.updateControllerType();
//		driverCtrl.setLeftStickXDeaband(0.2);
//		driverCtrl.setLeftStickYDeadban.d(0.2);
//		driverCtrl.setRightStickXDeadband(0.15);
		intake.set(0);
	}

	@Override
	public void teleopPeriodic() {
		drivetrain.update();
		double leftY = 0, leftX = 0, spin = 0;

		double rawLeftY = Robot.scaleJoystickInput(-driverCtrl.getLeftStickY(), .0, .8, .5);
		double rawLeftX = Robot.scaleJoystickInput(-driverCtrl.getLeftStickX(), .0, .8, .5);
		double rawRightX = Robot.scaleJoystickInput(-driverCtrl.getRightStickX(), .0, .8, .5);
		SmartDashboard.putNumber(" rawLeftY", rawLeftY);
		SmartDashboard.putNumber(" rawLeftX", rawLeftX);


		rawRightX = scaleTurningPower(rawRightX, rawLeftX, rawLeftY, .2, 0);
		SmartDashboard.putNumber(" rawRightX", rawRightX);

		if (drivetrain.getOutputMode() == Drivetrain.OutputMode.PERCENT) {
			leftY = rawLeftY * TRANSLATE_SCALE;
			leftX = rawLeftX * TRANSLATE_SCALE;
			spin = rawRightX * ROTATE_SCALE;
		} else if (drivetrain.getOutputMode() == Drivetrain.OutputMode.VELOCITY) {
			leftY = rawLeftY * Drivetrain.MAX_MPS * VEL_TRANSLATE_SCALE;
			leftX = rawLeftX * Drivetrain.MAX_MPS * VEL_TRANSLATE_SCALE;
			spin = rawRightX * Drivetrain.MAX_RPS * VEL_ROTATE_SCALE;
		}

//		if (!driverCtrl.getRightBumper()) {
		double leftXGain = 1;
		double leftYGain = 1;
//		}
		double gainDiff = 1 - leftXGain;
		double newGain = driverCtrl.getRightTrigger() * gainDiff;
		leftXGain += newGain;
		leftYGain += newGain;

		if (driverCtrl.getRightBumper()) {
			leftXGain = 1;
			leftYGain = 1;
		}

		leftX *= leftXGain;
		leftY *= leftYGain;


		boolean holdHeading = driverCtrl.a();
		if (driverCtrl.aPressed()) {
			headingToHold = 0;
		}

		ChassisSpeeds speeds;
		if (drivetrain.isFieldOriented()) {
			speeds = ChassisSpeeds.fromFieldRelativeSpeeds(leftY, leftX, spin, Rotation2d.fromDegrees(-drivetrain.getHeading()));
		} else {
			speeds = new ChassisSpeeds(leftY, leftX, spin);
		}

		if (driverCtrl.getDPadUp()) {
			drivetrain.zeroGyro();
		}

		if (driverCtrl.x()) {
			drivetrain.pointZero();
			return;
		}
		if (driverCtrl.b()) {
			intake.setServo(1.0);
		}
		if (driverCtrl.y()) {
			intake.setServo(0.0);
		}

		if (driverCtrl.getLeftStickMagnitude() < 0.1 && Math.abs(driverCtrl.getRightStickX()) < 0.1) {
			drivetrain.drive(new ChassisSpeeds(0, 0, 0));
		} else {
			if (holdHeading) {
				drivetrain.driveHoldHeading(speeds, Rotation2d.fromDegrees(headingToHold).getDegrees());
			} else if (driverCtrl.b()) {
				drivetrain.driveAboutCoR(
						speeds,
						new Translation2d(
								Drivetrain.WHEELBASE_WIDTH_M / 2 + Drivetrain.BUMPER_WIDTH + Units.inchesToMeters(12),
								0));
			} else {
				drivetrain.drive(speeds);
			}
		}
	}


	@Override
	public void disabledInit() {
		secondsElapsed = 0;
	}

	@Override
	public void disabledPeriodic() {
	}
}
