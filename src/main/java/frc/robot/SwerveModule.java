/*----------------------------------------------------------------------------*/

/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import com.ctre.phoenix.motorcontrol.*;
import com.revrobotics.CANAnalog;
import com.revrobotics.CANAnalog.AnalogMode;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import edu.wpi.first.wpilibj.DutyCycleEncoder;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.utils.EfficentFalcon;

import static com.revrobotics.CANSparkMaxLowLevel.MotorType.kBrushless;
import static frc.robot.SwerveModule.ModuleConstants.*;

/**
 * Represents a swerve module
 */
class SwerveModule {
	private final double encoderOffset;
	private CANSparkMax rotationMotor;
	private EfficentFalcon throttleMotor;
	private CANAnalog rotateAnalogEncoder;
	private CANPIDController rotationPIDController;
	private DutyCycleEncoder encoder;
	private SimpleMotorFeedforward motorFeedforward;
	private Drivetrain.Modules module;
	private double targetAngle = 0;
	private double targetThrottle = 0;
	private double previousError = 0;
	private double previousTimestamp = 0;
	private double integral = 0;
	private boolean usingModuleState = false;
	private double desiredVelocity;
	private double error = 0;
	private double throttleError = 0;

	/**
	 * Constructor for the module IDs
	 *
	 * @param rotateID   CAN ID for the motor that spins the module
	 * @param throttleID CAN ID for the motor that controls the linear speed
	 */
	SwerveModule(final short rotateID, final short throttleID, final short encoderID, boolean inverse, Drivetrain.Modules m) {
		rotationMotor = new CANSparkMax(rotateID, kBrushless);
		throttleMotor = new EfficentFalcon(throttleID);
		encoder = new DutyCycleEncoder(encoderID);

		setupThrottleMotor();

		rotationPIDController = rotationMotor.getPIDController();
		rotateAnalogEncoder = rotationMotor.getAnalog(AnalogMode.kAbsolute);

		rotationPIDController.setFeedbackDevice(rotateAnalogEncoder);
		rotationPIDController.setOutputRange(Drivetrain.DriveConstants.ROTATE_DEADBAND, Drivetrain.DriveConstants.ROTATE_MAX);
		rotationPIDController.setP(ModuleConstants.rotateP);
		rotationPIDController.setI(ModuleConstants.rotateI);
		rotationPIDController.setD(ModuleConstants.rotateD);

		this.module = m;

		throttleMotor.setInverted(inverse);

		encoderOffset = getEncoderOffset();

		motorFeedforward = new SimpleMotorFeedforward(0.5, 2.4);
	}

	/**
	 * Gets degrees from ticks
	 *
	 * @param ticks to be converted
	 * @return degrees
	 */
	@SuppressWarnings("divzero")
	private static double ticksToDegrees(final double ticks) {
		// return ticks == 0 ? 0 : ticks / ModuleConstants.rotationTicksPerRev * 360;
		return ticks == 0 ? 0 : ticks / ModuleConstants.rotationTicksPerRev * 360;
	}

	/**
	 * Gets ticks from degrees
	 *
	 * @param degrees to be converted
	 * @return ticks
	 */
	@SuppressWarnings("unused")
	static double degreesToTicks(final double degrees) {
		// return degrees == 0 ? 0 : degrees / 360 * ModuleConstants.rotationTicksPerRev;
		return degrees == 0 ? 0 : degrees / 360 * ModuleConstants.rotationTicksPerRev;
	}

	/**
	 * Checks if degrees is greater than 360 or less than 0 then returns degrees
	 *
	 * @param degrees bad degrees
	 * @return manufactured degrees
	 */
	@SuppressWarnings("unused")
	static double doubleDegreesToDegrees(final double degrees) {
		if (degrees < 0) {
			return 360 - degrees;
		} else if (degrees > 360) {
			return degrees - 360;
		}
		return degrees;
	}

	void setPercent(SwerveModuleState moduleState) {
		usingModuleState = true;
		moduleState = SwerveModuleState.optimize(moduleState, Rotation2d.fromDegrees(getCurrentAngle()));
		throttleMotor.set(TalonFXControlMode.PercentOutput, moduleState.speedMetersPerSecond);
		if (Math.abs(moduleState.speedMetersPerSecond) > 0.015) {
			setTargetAngle(moduleState.angle.getDegrees());
		}
	}

	void setManual(double throttleSpeed, double rotateSpeed) {
		throttleMotor.set(TalonFXControlMode.PercentOutput, throttleSpeed);
		rotationMotor.set(rotateSpeed);
	}

	/**
	 * Gets target throttle
	 */
	double getTargetThrottle() {
		return this.targetThrottle;
	}

	/**
	 * Sets the target throttle for update() to make the motor
	 *
	 * @param throttle should be in between -1 and 1
	 */
	void setTargetThrottle(final double throttle) {
		usingModuleState = false;
		this.targetThrottle = throttle;
	}

	/**
	 * (hopefully) Gets the encoder's 360 angle
	 *
	 * @return the current wheel's angle
	 */
	double getCurrentAngle() {
//		double value = ticksToDegrees(getEncoderValue());
//		return  Math.abs(value - 180) > 0.5 ? value % 180 : value;
		return ticksToDegrees(getEncoderValue());
	}

	double getEncoderValue() {
		return encoder.get() - encoderOffset;
	}

	public void pointZero(){
		setTargetAngle(0);
	}

	/**
	 * Gets the target angle setpoint for the rotation motor
	 *
	 * @return angle setpoint
	 */
	@SuppressWarnings("unused")
	double getTargetAngle() {
		return this.targetAngle;
	}

	/**
	 * Sets the target angle for the rotate motor to go to
	 *
	 * @param angle unbounded by 180
	 */
	void setTargetAngle(final double angle) {
		usingModuleState = false;
		this.targetAngle = angle;
	}

	double getVelocity() {
		return throttleMotor.getVelocity();
	}

	void setVelocity(SwerveModuleState moduleState) {
		usingModuleState = true;
		moduleState = SwerveModuleState.optimize(moduleState, Rotation2d.fromDegrees(getCurrentAngle()));
		desiredVelocity = moduleState.speedMetersPerSecond;
		throttleMotor.setVelocity(desiredVelocity);
		this.throttleError = throttleMotor.getVelErrorMetPerSec();
		if (Math.abs(moduleState.speedMetersPerSecond) > 0.015) {
			setTargetAngle(moduleState.angle.getDegrees());
		}
	}

	double getEncoderOffset() {
		switch (this.module) {
			case FRONT_LEFT:
				return FL_ENC_OFFSET;
			case FRONT_RIGHT:
				return FR_ENC_OFFSET;
			case BACK_LEFT:
				return BL_ENC_OFFSET;
			case BACK_RIGHT:
				return BR_ENC_OFFSET;
			default:
				return 0;
		}
	}

	SwerveModuleState getModuleState() {
		return new SwerveModuleState(getVelocity(), Rotation2d.fromDegrees(getCurrentAngle()));
	}

	void update() {
		final double currentAngle = getCurrentAngle();
		final double currentTime = Timer.getFPGATimestamp();
		final double dt = currentTime - this.previousTimestamp;
		this.previousTimestamp = currentTime;
		error = Rotation2d.fromDegrees(targetAngle).minus(Rotation2d.fromDegrees(currentAngle)).getDegrees();

		this.integral += (error * dt);
		final double derivative = (error - this.previousError) / dt;

		final double rotateOutput = (ModuleConstants.rotateP * error) + (ModuleConstants.rotateI * this.integral)
				+ (ModuleConstants.rotateD * derivative);

		this.previousError = error;
		this.rotationMotor.set(rotateOutput);
//		this.throttleMotor.set(TalonFXControlMode.Current, this.targetThrottle);
	}

	void updateSmart() {
		SmartDashboard.putNumber(module.name() + " PID error", error);
		SmartDashboard.putNumber(module.name() + " target angle", this.targetAngle);
		SmartDashboard.putNumber(module.name() + " encoder value", getEncoderValue());
		SmartDashboard.putNumber(module.name() + " pretty encoder angle", Rotation2d.fromDegrees(getCurrentAngle()).rotateBy(new Rotation2d(0)).getDegrees());
		SmartDashboard.putNumber(module.name() + " exact encoder angle", getCurrentAngle());
		SmartDashboard.putNumber(module.name() + " Velocity", getVelocity());
		SmartDashboard.putNumber(module.name() + " Desired Velocity", this.desiredVelocity);
		SmartDashboard.putNumber(module.name() + " Velocity Error", this.throttleError);
		SmartDashboard.putNumber(module.name() + " Raw Velocity", this.throttleMotor.getSelectedSensorVelocity());
		double rawSetpoint = 0;
		if (!RobotState.isDisabled()) {
			rawSetpoint = this.throttleMotor.getClosedLoopTarget();
		}
		SmartDashboard.putNumber(module.name() + " Raw Setpoint", rawSetpoint);
	}

	public void zeroEncoder() {
		encoder.reset();
	}

	/**
	 * Resets integral term to 0;
	 */
	public void resetIntegral() {
		this.integral = 0;
	}

	private void setupThrottleMotor() {
		throttleMotor.enableVoltageCompensation(true);
		throttleMotor.configVoltageCompSaturation(12);
		throttleMotor.setNeutralMode(NeutralMode.Brake);
		throttleMotor.setPositionConversionFactor(((Math.PI * WHEEL_DIAMETER_M) / GEAR_RATIO) / 2048);
		throttleMotor.setVelConversionFactor((10 * Math.PI * WHEEL_DIAMETER_M) / (2048 * GEAR_RATIO));
		throttleMotor.setSelectedSensorPosition(0);
		throttleMotor.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 70, 3, 1));
		throttleMotor.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 40, 3, 1));
		throttleMotor.config_kP(0, 0.01);
		throttleMotor.config_kI(0, 0);
		throttleMotor.config_kD(0, 0);
		throttleMotor.config_kF(0, 0.05);
	}

	/**
	 * Constants for the module
	 */
	static final class ModuleConstants {
		public static final double WHEEL_DIAMETER_CM = 11.7475;
		public static final double WHEEL_DIAMETER_M = WHEEL_DIAMETER_CM / 100;
		public static final double WHEEL_CIRCUMFERENCE_M = 2 * Math.PI * (WHEEL_DIAMETER_M / 2);
		public static final double GEAR_RATIO = 6;

		static final double rotationTicksPerRev = 1;
		@SuppressWarnings("unused")
		static final double throttleTicksPerRev = 0;
		static final double FL_ENC_OFFSET = 0.77057559426439;
		static final double FR_ENC_OFFSET = 0.08386720209668;
		static final double BL_ENC_OFFSET = 0.253831731345793;
		static final double BR_ENC_OFFSET = 0.780738769518469;

//		static final double FL_ENC_OFFSET = 0.751652468791312;
//		static final double FR_ENC_OFFSET = 0.10048240251206;
//		static final double BL_ENC_OFFSET = 0.030245550756139 + -0.070695;
//		static final double BR_ENC_OFFSET = 0.268240506706013;

		// The direction facing forward may not have the encoder at 0
		// There's 2 sets of PID variables for tuning purposes
		static double defaultRotP = 0.01;
		static double defaultRotI = 0.00000;
		static double defaultRotD = 0;
		static double rotateP = defaultRotP;
		static double rotateI = defaultRotI;
		static double rotateD = defaultRotD;
		// this
		// exists to solve that problem

		static void setPID(final double p, final double i, final double d) {
			rotateP = p;
			rotateI = i;
			rotateD = d;
		}

		@SuppressWarnings("unused")
		public static void setPIDToDefaults() {
			rotateP = defaultRotP;
			rotateI = defaultRotI;
			rotateD = defaultRotD;
		}
	}
}
