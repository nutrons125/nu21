package frc.robot;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig;
import edu.wpi.first.wpilibj.trajectory.TrajectoryGenerator;
import edu.wpi.first.wpilibj.trajectory.TrajectoryUtil;
import edu.wpi.first.wpilibj.util.Units;
import org.json.JSONArray;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

public class RobotPath {
	private final String name;
	private final ArrayList<Translation2d> checkpoints = new ArrayList<>();
	private TrajectoryConfig trajectoryConfig = new TrajectoryConfig(Drivetrain.MAX_MPS, Drivetrain.MAX_ACCEL);
	private Trajectory trajectory;
	private Pose2d startPose, endPose;
	private double endVelocity = 0;

	{
		trajectoryConfig.addConstraint(Drivetrain.getInstance().getConstraint());
	}

	public RobotPath(Pose2d startPose, Pose2d endPose) {
		this.name = getClass().getSimpleName();
		this.startPose = startPose;
		this.endPose = endPose;
	}

	public RobotPath(String name, Pose2d startPose, Pose2d endPose) {
		this.name = name;
		this.startPose = startPose;
		this.endPose = endPose;
	}
	public void toFileWindows() {
		ObjectMapper mapper = new ObjectMapper();
		File checkpointFile = new File("checkpoints.json");
		File objectives = new File("endpts.json");
		File statesFile = new File("traj.json");
		List<Pose2d> poses = Arrays.asList(this.startPose, this.endPose);
		Trajectory trajectory = generateTrajectory();
		try {
			System.out.println("Writing");
			mapper.writeValue(statesFile, trajectory);
			System.out.println(String.format("Wrote to %s and %s", checkpointFile, statesFile));
			mapper.writeValue(objectives, poses);
			mapper.writeValue(checkpointFile, this.checkpoints);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
			e.printStackTrace();
		}
	}



	@SuppressWarnings("unchecked")
	public static Trajectory fromFile(String fileName) throws FileNotFoundException {
		String filePath = Filesystem.getDeployDirectory().getPath() + "/paths/output/output/" + fileName;
		Scanner scanner = new Scanner(new File(filePath));
		StringBuilder fileText = new StringBuilder();
		while (scanner.hasNext()) {
			fileText.append(scanner.nextLine());
		}
		JSONArray jsonArray = new JSONArray(fileText.toString());
		List<Object> objectList = jsonArray.toList();
		ArrayList<Trajectory.State> pathPoints = new ArrayList<>();
		for (Object object : objectList) {
			HashMap<String, Object> pathPointJSON = (HashMap<String, Object>) object;
			float time = ((BigDecimal) pathPointJSON.get("time")).floatValue();
			float velocity = ((BigDecimal) pathPointJSON.get("velocity")).floatValue();
			float acceleration = ((BigDecimal) pathPointJSON.get("acceleration")).floatValue();
			float curvature = ((BigDecimal) pathPointJSON.get("curvature")).floatValue();
			HashMap<String, Object> ppPose = (HashMap<String, Object>) pathPointJSON.get("pose");
			HashMap<String, Object> ppTranslation = (HashMap<String, Object>) ppPose.get("translation");
			float x = ((BigDecimal) ppTranslation.get("x")).floatValue();
			float y = ((BigDecimal) ppTranslation.get("y")).floatValue();
			HashMap<String, Object> pathPointRotation = (HashMap<String, Object>) ppPose.get("rotation");
			float radians = ((BigDecimal) pathPointRotation.get("radians")).floatValue();

			Pose2d pose = new Pose2d(new Translation2d(x, y), new Rotation2d(radians));
			Trajectory.State state = new Trajectory.State(time, velocity, acceleration, pose, curvature);
			pathPoints.add(state);
		}

		return new Trajectory(pathPoints);
	}

	public String getName() {
		return name;
	}

	public void setStart(Pose2d startPose) {
		this.startPose = startPose;
	}

	public void setEndPose(Pose2d endPose) {
		this.endPose = endPose;
	}

	public RobotPath addCheckpoint(Translation2d checkpoint) {
		checkpoints.add(checkpoint);
		return this;
	}

	public void setCheckpoints(Translation2d... checkpoints) {
		this.checkpoints.clear();
		Collections.addAll(this.checkpoints, checkpoints);
	}

	public void setCheckpoints(ArrayList<Translation2d> checkpoints) {
		this.checkpoints.clear();
		this.checkpoints.addAll(checkpoints);
	}

	public void setEndVelocity(double velocity) {
		trajectoryConfig.setEndVelocity(velocity);
	}

	public void setStartVel(double v) { trajectoryConfig.setStartVelocity(v);}

	public void setConfig(TrajectoryConfig config) {
		this.trajectoryConfig = config;
		this.trajectoryConfig.addConstraint(Drivetrain.getInstance().getConstraint());
	}

	public Trajectory generateTrajectory() {
		return TrajectoryGenerator.generateTrajectory(
				startPose,
				checkpoints,
				endPose,
				trajectoryConfig);
	}
}
