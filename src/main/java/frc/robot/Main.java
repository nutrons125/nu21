// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;

import static frc.robot.utils.Handy.f2m;
import static frc.robot.utils.Handy.makePose;

public final class Main {
	private Main() {
	}

	public static void main(String... args) {
		Pose2d startPose = makePose(0, 0, 0);

		RobotPath galaxyPath = new RobotPath(
				startPose,
				makePose(f2m(30.5 + 5), f2m((7.5 - 1.5) - 7.5), 0));

		galaxyPath.addCheckpoint(new Translation2d(f2m(7.5), f2m(7.5 - 7.5)));
		galaxyPath.addCheckpoint(new Translation2d(f2m(12.5), f2m(5 - 7.5)));
		galaxyPath.addCheckpoint(new Translation2d(f2m(16 + 1.5), f2m(12.5 - 7.5)));
		galaxyPath.toFileWindows();
		RobotBase.startRobot(Robot::new);
	}
}
