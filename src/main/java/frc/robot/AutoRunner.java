package frc.robot;

import edu.wpi.first.wpilibj.geometry.Rotation2d;
import frc.robot.autos.Auto;
import frc.robot.autos.AutoMode;

/*
TODO
Make a button for orbiting around a point
Make auto commands selectable from SendableChooser
 */
public class AutoRunner {
	private static AutoMode autoMode;
	private Auto auto = AutoMode.None.getAuto();

	public AutoRunner() {
	}

	public static Rotation2d getDesiredRotation() {
		return new Rotation2d();
	}

	public void setAuto(AutoMode autoMode) {
		AutoRunner.autoMode = autoMode;
		this.auto = autoMode.getAuto();
		this.auto.generate();
	}

	public void run() {
		this.auto.run();
	}
}
